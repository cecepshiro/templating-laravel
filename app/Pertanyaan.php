<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table='pertanyaan';
    protected $primaryKey='id';
    public $incrementing =true;
    public $timestamps=true; 
    const CREATED_AT = 'tanggal_dibuat';
    const UPDATED_AT = 'tanggal_diperbaharui'; 
    protected $fillabe = ['id','judul','isi','tanggal_dibuat','tanggal_diperbaharui','jawaban_tepat_id','profil_id'];
}
